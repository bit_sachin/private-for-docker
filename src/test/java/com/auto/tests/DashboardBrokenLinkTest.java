package com.auto.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import com.auto.base.TestCore;
import com.auto.pages.DashboardPage;

public class DashboardBrokenLinkTest extends TestCore {
	
	DashboardPage dashboardPage;
	
	@Test(priority=0)
	public void VerifyAllLink(){
		 dashboardPage = new DashboardPage(driver);
		 dashboardPage.checkResponsesOfAllLinks();
	}
	
	@Test(priority=1)
	public void VefifyH1Text(){
		
		Assert.assertTrue(dashboardPage.verifyH1Tag("Besuchen Sie uns auf"), "No H1 Tag appears on page");
	}

}
