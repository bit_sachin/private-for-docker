package com.auto.pages;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.auto.base.BasePage;
import com.auto.base.ILogLevel;
import com.auto.base.TestCore;
import com.auto.configProperties.ConfigProperties;
import com.auto.pageObject.DashboardPageObject;

public class DashboardPage extends BasePage{

	public DashboardPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	int i;
	
	public void checkResponsesOfAllLinks(){
		
		List<WebElement> links=driver.findElements(By.tagName(DashboardPageObject.tag_xpath));
		
		System.out.println("Total links are "+links.size());
		
		for(i=0;i<links.size();i++)
		{
			
			WebElement ele= links.get(i);
			
			String url=ele.getAttribute(DashboardPageObject.attribute_value);
			TestCore.excel.setCellData(ConfigProperties.Sheet_Name, ConfigProperties.URL_column, i+2, url);
	
			try 
		    {
		       URL urlConnection = new URL(url);
		       
		       HttpURLConnection httpURLConnect=(HttpURLConnection)urlConnection.openConnection();
		       
		       httpURLConnect.setConnectTimeout(3000);
		       
		       httpURLConnect.connect();
		       
		       if(httpURLConnect.getResponseCode()==200)
		       {
		           log(url+" - "+httpURLConnect.getResponseMessage(),ILogLevel.METHOD);
		           TestCore.excel.setCellData(ConfigProperties.Sheet_Name, ConfigProperties.Response_column, i+2, httpURLConnect.getResponseMessage());
		           TestCore.excel.setCellData(ConfigProperties.Sheet_Name, ConfigProperties.Result_column, i+2, "Pass");
		           
		        }
		      if(httpURLConnect.getResponseCode()!=200)  
		       {
		          	log(url+" - "+httpURLConnect.getResponseMessage(),ILogLevel.METHOD);
		          	TestCore.excel.setCellData(ConfigProperties.Sheet_Name, ConfigProperties.Response_column, i+2, httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
		          	TestCore.excel.setCellData(ConfigProperties.Sheet_Name, ConfigProperties.Result_column, i+2, "Fail");
		       }
		    } catch (Exception e) {
		       
		    }
			
		}
	}
	
	public boolean verifyH1Tag(String _text){
		
		boolean text = isElementPresent(By.xpath(DashboardPageObject.heading_xpath));
		
		if(text){
	
			String content = driver.findElement(By.xpath(DashboardPageObject.heading_xpath)).getText();
			if(content.contains(_text)){
				return true;
			}
			return false;
		
		}
		else{
			return false;
		}
	}

}
