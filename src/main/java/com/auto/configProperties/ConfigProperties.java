package com.auto.configProperties;

public class ConfigProperties {
	
	//Enter Sauce Lab Usernamer and Access Key
	public static final String USERNAME = "QA_User007";
	public static final String ACCESS_KEY = "5e8267af-9652-49b4-bc66-7373505caca8";

	//Enter crossBrowser value as "Local" to run on Local and "SauceLabs" to run on Sauce Labs
	public static String crossBrowser = "saucelabs";

	//Select OS: (windows / mac)
	public static String OS ="windows";

	//Enter URL:
	public static String site_url = "http://demo.code5.org/";

	//Select browser (firefox,chrome,IE,Safari)
	public static String browser_name = "chrome";

	//ImplictTime
	public static int time = 30;
	
	//Excel File column name
	public static String Excel_File_Name = "BrokenLink.xlsx";
	public static String Sheet_Name = "Sheet1";
	public static String URL_column = "URL";
	public static String Response_column = "Response";
	public static String Result_column = "Result";

}
