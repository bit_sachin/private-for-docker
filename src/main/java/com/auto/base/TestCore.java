package com.auto.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.ScreenshotException;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.auto.configProperties.ConfigProperties;

public class TestCore extends Page{

	static DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
	static Date date = new Date();
	public static Properties object = new Properties();
	public static Properties config = new Properties();
	public static WebDriver driver;
	public static String SCREENSHOT_FOLDER = "target/screenshots/";
	public static final String SCREENSHOT_FORMAT = ".png";
	protected String testUrl;
	private String targetBrowser;
	private String os;
	public static final String URL = "http://" + ConfigProperties.USERNAME + ":" + ConfigProperties.ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub";
	private String crossBrowser;
	public static Xls_Reader excel;
	
	@BeforeSuite
	public void setUpTestSuite() throws MalformedURLException{

		targetBrowser = ConfigProperties.browser_name;
		os = ConfigProperties.OS;
		crossBrowser = ConfigProperties.crossBrowser;
		
		if(crossBrowser.toLowerCase().equals("local")){

			if(os.toLowerCase().equals("windows")){
				if (targetBrowser.toLowerCase().contains("firefox") || targetBrowser.toLowerCase().contains("ff")){
					driver=new FirefoxDriver();
			}
				else if(targetBrowser.toLowerCase().contains("chrome")){
					System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//src//main//resources//driver_windows//chromedriver.exe"); 
					driver = new ChromeDriver();
				}

				else if(targetBrowser.toLowerCase().contains("IE")||targetBrowser.toLowerCase().contains("ie")){
					System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"//src//main//resources//driver_windows//IEDriverServer.exe");	
					driver = new InternetExplorerDriver();
				}

			}
		}
		

		else if(crossBrowser.toLowerCase().equals("saucelabs")){

			if(os.toLowerCase().equals("windows")){

				if (targetBrowser.toLowerCase().contains("firefox") || targetBrowser.toLowerCase().contains("ff")){

					DesiredCapabilities caps = DesiredCapabilities.firefox();
				    caps.setCapability("platform", "Windows 7");
				    caps.setCapability("version", "50.0");
				    caps.setCapability("name", "Broken Link Test");

					driver = new RemoteWebDriver(new URL(URL), caps);
					
				}
				else if(targetBrowser.toLowerCase().contains("chrome")){

					DesiredCapabilities caps = DesiredCapabilities.chrome();
				    caps.setCapability("platform", "Windows 7");
				    caps.setCapability("version", "54.0");
				    caps.setCapability("name", "Broken Link Test");
				 
				    driver = new RemoteWebDriver(new URL(URL), caps);
					
				}

				else if(targetBrowser.toLowerCase().contains("IE")||targetBrowser.toLowerCase().contains("ie")){

					DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
				    caps.setCapability("platform", "Windows 7");
				    caps.setCapability("version", "11.0");
				    caps.setCapability("name", "Broken Link Test");
				 
				    driver = new RemoteWebDriver(new URL(URL), caps);
					
				}

			

			}
			
		}
		driver.manage().window().maximize();
	}
		
	
			
		
			@BeforeTest(alwaysRun = true)
			public void fetchSuiteConfiguration(ITestContext testContext) {

				try{
					excel = new Xls_Reader(System.getProperty("user.dir")+"\\src\\main\\resources\\"+ ConfigProperties.Excel_File_Name);
					
					
				}catch(Exception e){}

				testUrl = ConfigProperties.site_url;
				System.out.println("----------" + testUrl + "----------");

			}

			/**
			 * WebDriver initialization
			 * 
			 * @return WebDriver object
			 * @throws IOException
			 * @throws InterruptedException
			 */
			@BeforeMethod(alwaysRun = true)
			public void setup(Method method) throws IOException, InterruptedException {

				// Open test url
				driver.get(testUrl);
				//Add implicit wait time
				driver.manage().timeouts().implicitlyWait(ConfigProperties.time, TimeUnit.SECONDS);


				log("--------------------------------------------------------", ILogLevel.TESTCASE);
				log("Test ["+method.getName()+"] Started", ILogLevel.TESTCASE);
				log("--------------------------------------------------------", ILogLevel.TESTCASE);

			}




			/**
			 * capture screenshot on test(pass/fail)
			 * 
			 * 
			 */




			@AfterMethod
			public void setScreenshot(ITestResult result) {

				if(!result.isSuccess()||result.isSuccess()){
					try {
						if ( driver != null) {
							File f = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
							try {
								FileUtils.copyFile(f, new File(SCREENSHOT_FOLDER+ result.getName() + SCREENSHOT_FORMAT)
										.getAbsoluteFile());
							} catch (IOException e) { 
								e.printStackTrace(); 
							}
						}

					} catch (ScreenshotException se) {
						se.printStackTrace();
					}catch(Exception e){
						e.printStackTrace();
					}
				}

			}

			@AfterSuite
			public void tearDown(){

				if (driver != null) {

					driver.close();
					driver.quit();   // driver close after test run 

				}
			}
		}


